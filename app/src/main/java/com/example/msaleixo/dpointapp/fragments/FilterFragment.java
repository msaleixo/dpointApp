package com.example.msaleixo.dpointapp.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.example.msaleixo.dpointapp.MainActivity;
import com.example.msaleixo.dpointapp.R;
import com.example.msaleixo.dpointapp.models.FilterData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FilterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FILTER_DATA_KEY = "filterData";

    // TODO: Rename and change types of parameters
    private FilterData filterData;

    private OnFragmentInteractionListener mListener;

    public static String TAG = "Dpoint";

    // Controles
    public EditText storeName;
    public SeekBar distanceBar;
    public SeekBar minDescBar;
    public SeekBar maxDescBar;
    public Spinner travelMode;
    public ArrayAdapter<CharSequence> adapter;

    public FilterFragment() {
        TAG = MainActivity.TAG;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param filterData Parameter 1.
     * @return A new instance of fragment FilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFragment newInstance(FilterData filterData) {
        Log.d(TAG, "newIntance Filter");
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putSerializable(FILTER_DATA_KEY, filterData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate Filter");
        if (getArguments() != null) {
            filterData = (FilterData) getArguments().getSerializable(FILTER_DATA_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView Filter");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter, container, false);

        storeName = view.findViewById(R.id.storeName);
        distanceBar = view.findViewById(R.id.distanceBar);
        minDescBar = view.findViewById(R.id.minDescBar);
        maxDescBar = view.findViewById(R.id.maxDescBar);
        travelMode = view.findViewById(R.id.travelMode);

        adapter = ArrayAdapter.createFromResource(getContext(), R.array.travel_mode_items, android.R.layout.simple_spinner_item);
        if (filterData != null){
            storeName.setText(filterData.getTienda());
            distanceBar.setProgress(filterData.getDistancia());
            minDescBar.setProgress(filterData.getMinDesc());
            maxDescBar.setProgress(filterData.getMaxDesc());
            travelMode.setSelection(adapter.getPosition(filterData.getTravelMode()));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach Filter");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach Filter");
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }
}
