package com.example.msaleixo.dpointapp.apis;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * API Client going to be used while connecting DataClient.
 *
 * @author DataClient
 * @version v1.0
 *
 */
public class ApiClient {
    private static DataClient REST_CLIENT;
    private static final String API_URL = "http://msaleixo2012.000webhostapp.com";

    static {
        setupRestClient();
    }

    private ApiClient() {}

    public static DataClient get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //Uncomment these lines below to start logging each request.

        /*
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        */

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();


        REST_CLIENT = retrofit.create(DataClient.class);
    }
}
