package com.example.msaleixo.dpointapp.models;

import java.io.Serializable;

public class FilterData implements Serializable {
    private String tienda;

    private Integer distancia;

    private Integer minDesc;

    private Integer maxDesc;

    private String travelMode;

    public FilterData(String tienda, Integer distancia, Integer minDesc, Integer maxDesc, String travelMode) {
        this.tienda = tienda;
        this.distancia = distancia;
        this.minDesc = minDesc;
        this.maxDesc = maxDesc;
        this.travelMode = travelMode;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

    public Integer getMinDesc() {
        return minDesc;
    }

    public void setMinDesc(Integer minDesc) {
        this.minDesc = minDesc;
    }

    public Integer getMaxDesc() {
        return maxDesc;
    }

    public void setMaxDesc(Integer maxDesc) {
        this.maxDesc = maxDesc;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }
}
