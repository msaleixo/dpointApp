package com.example.msaleixo.dpointapp;

import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.msaleixo.dpointapp.apis.ApiClient;
import com.example.msaleixo.dpointapp.fragments.FilterFragment;
import com.example.msaleixo.dpointapp.fragments.MapFragment;
import com.example.msaleixo.dpointapp.models.Cupon;
import com.example.msaleixo.dpointapp.models.FilterData;
import com.example.msaleixo.dpointapp.models.MyLocation;
import com.example.msaleixo.dpointapp.models.Post;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FilterFragment.OnFragmentInteractionListener {

    public static final String TAG = "Dpoint";

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location and default zoom to use when location permission is
    // not granted.
    private final MyLocation mDefaultLocation = new MyLocation(28.540158, -81.386669);
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_LOCATION = "location";
    private static final String KEY_FILTER_DATA = "filterData";

    // Cupones
    private List<Cupon> cupones = new ArrayList();

    // FilterData
    private FilterData filterData;
    private MyLocation myLocation;
    private FilterFragment filterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            // Retrieve location and camera position from saved instance state.
            if (savedInstanceState != null) {
                Log.d(TAG, "Recuperando Instancia");
                mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
                filterData = (FilterData) savedInstanceState.getSerializable(KEY_FILTER_DATA);
            } else {
                // Se cargan los datos por defecto
                filterData = new FilterData("", 1000, 0, 100, "DRIVING");
            }

            Log.d(TAG, "Iniciando Location Service");
            // Construct a FusedLocationProviderClient.
            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

            initGetLocation();
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void initGetLocation() {
        getLocationPermission();
        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "Back Presionado");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_filter) {
            Log.d(TAG, "Iniciando FilterFragment");
            filterFragment = FilterFragment.newInstance(filterData);
            loadFragment(filterFragment);
        } else if (id == R.id.nav_map) {
            initMap();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        Log.d(TAG, "Getting Location Permission");
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "PERMISSION_GRANTED");
            mLocationPermissionGranted = true;
        } else {
            Log.d(TAG, "Requesting Permission");
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "Request Result");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "PERMISSION_GRANTED");
                    mLocationPermissionGranted = true;
                } else {
                    Log.d(TAG, "PERMISSION_DENIED");
                }
            }
        }
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "SAVING STATE");
        outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
        outState.putSerializable(KEY_FILTER_DATA, filterData);
        super.onSaveInstanceState(outState);
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Log.d(TAG, "Getting Device Location");
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        Log.d(TAG, "Completed Listener");
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Ubicacion Detectada");
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            myLocation = new MyLocation(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            myLocation = mDefaultLocation;
                        }
                        loadCupons();
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e(TAG,"Exception: " + e.getMessage());
        }
    }

    private void loadCupons(){
        Log.d(TAG, "Calling Cupons");
        Post postData = new Post(filterData, myLocation);
        Call<List<Cupon>> call = ApiClient.get().getCupons(postData);
        call.enqueue(new Callback<List<Cupon>>() {
            @Override
            public void onResponse(Call<List<Cupon>> call, Response<List<Cupon>> response) {
                Log.d(TAG, "Response Received");

                if (response.isSuccessful()) {
                    Log.d(TAG, "Cupons Fetched");
                    cupones = response.body();
                    initMap();
                } else {
                    Log.e(TAG, String.format("Response Error Code: %d", response.code()));
                    try {
                        Log.e(TAG, String.format("Response Body: %s", response.errorBody().string()));
                    } catch (IOException e) {
                        Log.e(TAG, "Exception", e);
                    }
                    loadJson();
                }
            }

            @Override
            public void onFailure(Call<List<Cupon>> call, Throwable t) {
                Log.e(TAG, "Exception: %s", t);
                loadJson();
            }
        });
    }

    private void loadJson(){
        Log.d(TAG, "Calling Json");
        Call<List<Cupon>> call = ApiClient.get().getJson();
        call.enqueue(new Callback<List<Cupon>>() {
            @Override
            public void onResponse(Call<List<Cupon>> call, Response<List<Cupon>> response) {
                Log.d(TAG, "Response Received");

                if (response.code() == 200) {
                    Log.d(TAG, "Json Fetched");
                    cupones = response.body();
                    initMap();
                } else {
                    Log.e(TAG, String.format("Response Error Code: %d", response.code()));
                    Log.e(TAG, String.format("Response Body: %s", response.body()));
                }
            }

            @Override
            public void onFailure(Call<List<Cupon>> call, Throwable t) {
                Log.e(TAG, "Exception: %s", t);
            }
        });
    }

    private void initMap() {
        try {
            Log.d(TAG, "Iniciando Fragmento del Mapa");
            loadFragment(MapFragment.newInstance(cupones, myLocation));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private void loadFragment(Fragment mapFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, mapFragment);
        transaction.commit();
    }

    public void saveBtnClick(View view) {
        filterData.setTienda(filterFragment.storeName.getText().toString());
        filterData.setDistancia(filterFragment.distanceBar.getProgress());
        filterData.setMinDesc(filterFragment.minDescBar.getProgress());
        filterData.setMaxDesc(filterFragment.maxDescBar.getProgress());
        filterData.setTravelMode((String) filterFragment.adapter.getItem(filterFragment.travelMode.getSelectedItemPosition()));
        initGetLocation();
    }

    public void cancelBtnClick(View view) {
        initMap();
    }
}
