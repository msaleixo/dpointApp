package com.example.msaleixo.dpointapp.fragments;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.msaleixo.dpointapp.MainActivity;
import com.example.msaleixo.dpointapp.R;
import com.example.msaleixo.dpointapp.models.Cupon;
import com.example.msaleixo.dpointapp.models.MyLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String CUPONS_KEY = "cupons";
    private static final String MY_LOCATION_KEY = "myLocation";

    // TODO: Rename and change types of parameters
    private List<Cupon> cupones;
    private MyLocation myLocation;

    public static String TAG;
    private MapView mMapView;
    private GoogleMap mMap;
    private LatLng mUserLocation;
    private SupportMapFragment mapFragment;

    public MapFragment() {
        TAG = MainActivity.TAG;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param cupones Parameter 1.
     * @param myLocation Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(List<Cupon> cupones, MyLocation myLocation) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putString(CUPONS_KEY, new Gson().toJson(cupones));
        args.putSerializable(MY_LOCATION_KEY, myLocation);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Creating Fragment");
        if (getArguments() != null) {
            Type listType = new TypeToken<List<Cupon>>(){}.getType();
            cupones = new Gson().fromJson(getArguments().getString(CUPONS_KEY), listType);
            myLocation = (MyLocation) getArguments().getSerializable(MY_LOCATION_KEY);
            mUserLocation = new LatLng(myLocation.getLat(), myLocation.getLng());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "Inflating Layout");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        // don't recreate fragment everytime ensure last map location/state are maintained
        if (mapFragment == null) {
            Log.d(TAG, "Getting mapFragment");
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(this);
        }

        // R.id.map is a FrameLayout, not a Fragment
        getChildFragmentManager().beginTransaction().replace(R.id.mapView, mapFragment).commit();

        return view;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            Log.d(TAG, "Map Ready");
            mMap = googleMap;

            Log.d(TAG, "Agregando MyLocation Marcador");
            mMap.addMarker(new MarkerOptions()
                    .position(mUserLocation)
                    .title("My Location")
                    .icon(BitmapDescriptorFactory.fromBitmap(resToBitmap(R.drawable.my_loc))));

            LatLngBounds bounds = new LatLngBounds(mUserLocation, mUserLocation);

            for (Cupon cupon : cupones){
                LatLng position = new LatLng(cupon.getLat(), cupon.getLng());
                mMap.addMarker(new MarkerOptions()
                        .position(position)
                        .title(cupon.getPromoNombre())
                        .icon(BitmapDescriptorFactory.fromBitmap(resToBitmap(getIcon(cupon.getPorcDesc()))))
                        .snippet(getSnippet(cupon)));
                bounds = bounds.including(position);
            }

            Log.d(TAG, "Moviendo Camara");
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30));
        } catch (Exception e) {
            Log.e(TAG, "Exception", e);
        }
    }

    private String getSnippet(Cupon cupon) {
        StringBuilder s = new StringBuilder();
        s.append(cupon.getPorcDesc());
        s.append("% - ");
        s.append(cupon.getDistancia());
        s.append(" mtrs - Tienda ");
        s.append(cupon.getTienda().getNombre());

        return s.toString();
    }

    private Bitmap resToBitmap(int resource){
        ImageView img = new ImageView(getContext());
        img.setImageResource(resource);
        return ((BitmapDrawable)img.getDrawable()).getBitmap();
    }

    private int getIcon(int porcDesc){
        if (porcDesc < 17){
            return R.drawable.mark1;
        } else if (porcDesc < 34){
            return R.drawable.mark2;
        } else if (porcDesc < 51){
            return R.drawable.mark3;
        } else if (porcDesc < 68){
            return R.drawable.mark4;
        } else if (porcDesc < 85){
            return R.drawable.mark5;
        } else {
            return R.drawable.mark6;
        }
    }

}
