package com.example.msaleixo.dpointapp.models;

public class Post {

    private FilterData filter;

    private MyLocation myloc;

    public Post(FilterData filter, MyLocation myloc) {
        this.filter = filter;
        this.myloc = myloc;
    }

    public FilterData getFilter() {
        return filter;
    }

    public void setFilter(FilterData filter) {
        this.filter = filter;
    }

    public MyLocation getMyloc() {
        return myloc;
    }

    public void setMyloc(MyLocation myloc) {
        this.myloc = myloc;
    }
}
