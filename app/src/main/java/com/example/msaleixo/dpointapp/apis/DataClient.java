package com.example.msaleixo.dpointapp.apis;

import com.example.msaleixo.dpointapp.models.Cupon;
import com.example.msaleixo.dpointapp.models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface DataClient {

    @GET("/dpoint/data/cupones.json")
    Call<List<Cupon>> getJson();

    @POST("/dpoint/data/server.php")
    Call<List<Cupon>> getCupons(@Body Post postData);
}
