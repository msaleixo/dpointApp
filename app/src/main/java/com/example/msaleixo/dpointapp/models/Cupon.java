
package com.example.msaleixo.dpointapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cupon {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("lat")
    @Expose
    private Double lat;

    @SerializedName("lng")
    @Expose
    private Double lng;

    @SerializedName("porc_desc")
    @Expose
    private Integer porcDesc;

    @SerializedName("promo_nombre")
    @Expose
    private String promoNombre;

    @SerializedName("promo_detalle")
    @Expose
    private String promoDetalle;

    @SerializedName("vence")
    @Expose
    private String vence;

    @SerializedName("hora")
    @Expose
    private String hora;

    @SerializedName("monto_min")
    @Expose
    private Integer montoMin;

    @SerializedName("moneda")
    @Expose
    private String moneda;

    @SerializedName("precio_orig")
    @Expose
    private Double precioOrig;

    @SerializedName("precio_desc")
    @Expose
    private Double precioDesc;

    @SerializedName("tienda")
    @Expose
    private Tienda tienda;

    @SerializedName("distancia")
    @Expose
    private Integer distancia;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(Integer porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getPromoNombre() {
        return promoNombre;
    }

    public void setPromoNombre(String promoNombre) {
        this.promoNombre = promoNombre;
    }

    public String getPromoDetalle() {
        return promoDetalle;
    }

    public void setPromoDetalle(String promoDetalle) {
        this.promoDetalle = promoDetalle;
    }

    public String getVence() {
        return vence;
    }

    public void setVence(String vence) {
        this.vence = vence;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Integer getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Double getPrecioOrig() {
        return precioOrig;
    }

    public void setPrecioOrig(Double precioOrig) {
        this.precioOrig = precioOrig;
    }

    public Double getPrecioDesc() {
        return precioDesc;
    }

    public void setPrecioDesc(Double precioDesc) {
        this.precioDesc = precioDesc;
    }

    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

}
